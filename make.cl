
(declaim (optimize speed))

(setf ql:*quicklisp-home* (asdf::truenamize #P"quicklisp/"))

(ql:update-all-dists)
(ql:quickload :clx)
(ql:quickload :cl-ppcre)

(load "iron-maiden.cl")

(in-package :im-user)

(defun make ()
  (sb-ext:save-lisp-and-die
    "iron-maiden"
    :toplevel #'main
    :executable t
    :purify t
    :compression 9))

(make)
