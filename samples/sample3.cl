
; ignore key (code=30)
(process
  (unless (= 30 (raw-code-of (yield)))
    (pass)))


; if key30
;   then send key40
;   else execute default action.
(process
  (if (= 30 (raw-code-of (yield)))
    (send-key 40)
    (do-default)))
(process
  (let ((ev (raw-code-of (yield))))
    (if (= 30 ev)
      (send-key 40)
      (do-default ev))))
