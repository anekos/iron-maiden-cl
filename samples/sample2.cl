

(for (device "HHK")
  (key-map #<a> #<q>)
  (key-swap #<semicolon> #<s-semicolon>))

(for (device "TypeMatrix")
  (key-map (remap #<c>) #<d>)


; (on-device "HHK"
;   (key-map ...))
; ==
; (on-event im ev device-name
;   (let* ((ev-data (im-read ev)))
;     (when (= device-name "HHK")
;       (key-map ...))))

; (key-swap #<semicolon> #<s-semicolon>)
; ==
; (list (key-map #<semicolon> #<s-semicolon>)
;       (key-map #<s-semicolon> #<semicolon>))
