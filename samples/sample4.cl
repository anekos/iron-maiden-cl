
(defprocess foo ev
  (when (= (yield) 11)
    (send-event)
    (do-next))
  (do-next))

(run-to-the-hills

  (define-keyboard-device HHK #P"/dev/input/by-id/usb-Topre_Corporation_HHKB_Professional-event-kbd")
  (define-mouse-device TypeMatrix #P"/dev/input/by-id/usb-Type_Matrix-event-kbd")

  (for-device (HHK TypeMatrix)
    (key-map #<a> #<q>)
    (key-map #<b> #<c>)
    (key-map #<z><z> #<c>)
    (rhythm-map (#<ctrl> 1 #<ctrl> 2 #<ctrl>) #<alt-i>)
    (process
      (let ((ev (raw-code-of (yield))))
        (if (= 30 ev)
          (send-key 40)
          (do-next ev)))))

  (for-device TypeMatrix
    (...)))
