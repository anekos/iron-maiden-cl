; vim: set lispwords+=tap,for-device,for-window-class,def-event-co-routine,when-let :

(in-package :im-user)

; variables {{{

(defvar *output-mode* nil)
(defvar *output-mode-put-syn* nil)
(defvar *foot-pedal-pressed* nil)

; }}}

; utils {{{

(defun command-path (name)
  (dolist (dir *path*)
    (let ((path (merge-pathnames name dir)))
      (when (probe-file path)
        (return (namestring path))))))

(defun run (cmd &rest args)
  (if-let (cmd-path t)
    (sb-ext:run-program
      "/bin/sudo"
      `("-u" ,*user* ,cmd ,@(alexandria:flatten args))
      :wait nil
      :output t
      :error t
      :if-error-exists t
      :search t
      :environment (list (format nil "PATH=~{~A~^:~}" *path*)
                         (format nil "DISPLAY=~A" ":0.0")))
    (format t "command not found: ~A~%" cmd)))

(defun toggle-xmonad-pad (pad-name)
  (run "xc" "scratchpad" pad-name))

(defun slock ()
  (run "slock"))

(defun remocon (sub)
  (run "remocon" sub))

(defun zip-with (func &rest lists)
  (apply #'mapcar (cons func lists)))

(defmacro key-map-to-action (lhs &rest action)
  (with-gensyms (history lhs-c wait-for last-key)
    (eval `(defvar ,lhs-c (im-key:key-pair-list-from-text ,lhs :symbol-modifiers t)))
    (eval `(defvar ,history (im-ring-buffer:make-ring-buffer (length ,lhs-c))))
    (eval `(defvar ,wait-for nil))
    (eval `(defvar ,last-key (caar (last ,lhs-c))))
    `(if ,wait-for
       (when-key-press
         (when (= code ,last-key)
           (equal-case value
             (value-repeat
               ,@action)
             (value-up
               (setf ,wait-for nil)))
           (next-event)))
       (let ((modifiers ($ im keyboard-state im-state:current-modifires)))
         (when-key-down
           (im-ring-buffer:put-to-buffer ,history (cons code modifiers))
           (when (equalp (coerce ,lhs-c 'vector) (im-ring-buffer:get-straight ,history))
             ,@action
             (im-ring-buffer:clear-all-buffer ,history)
             (setf ,wait-for t)
             (next-event)))))))

(defmacro key-map-to-key (lhs rhs)
  `(key-map-to-action ,lhs (press-key im ,rhs)))

(defmacro key-map (lhs rhs &rest rhs-rest)
  (if (stringp rhs)
    `(key-map-to-key ,lhs ,rhs)
    `(key-map-to-action ,lhs ,rhs ,@rhs-rest)))

(defmacro x-and-y (original-key change-key modifier-key)
  (with-gensyms (pressing any-key-pressed)
    (eval `(defvar ,pressing nil))
    (eval `(defvar ,any-key-pressed nil))
    `(when-key-press
       (if (= code ,original-key)
         (equal-case
           value
           (imv:value-down
             (setf ,pressing t)
             (next-event))
           (imv:value-repeat
             (next-event))
           (imv:value-up
             (if ,any-key-pressed
               (progn
                 (send-event im imv:ev-key ,modifier-key imv:value-up)
                 (setf ,any-key-pressed nil))
               (progn
                 (let ((send-key (or ,change-key ,original-key)))
                   (send-event im imv:ev-key send-key imv:value-down)
                   (send-event im imv:ev-key send-key imv:value-up))))
             (setf ,pressing nil)
             (next-event)))
         (when (and (= value imv:value-down) ,pressing (not ,any-key-pressed))
           (send-event im imv:ev-key ,modifier-key imv:value-down)
           (setf ,any-key-pressed t))))))

(defun toggle-output-mode ()
  (setf *output-mode* (not *output-mode*))
  (debug-out nil "toggle output-mode: ~A" *output-mode*))

(defmacro set-signal-handler (signo &body body)
  (let ((handler (gensym "HANDLER")))
    `(progn
       (cffi:defcallback ,handler :void ((signo :int))
         (declare (ignore signo))
         ,@body)
       (cffi:foreign-funcall "signal" :int ,signo :pointer (cffi:callback ,handler)))))

(defmacro reverse-pointer ()
  `(when (= event-type imv:ev-rel)
     (modify-event-value ev 'value (lambda (v) (* -1 v)))))

; }}}

; counter {{{

(defvar *key-counters-save-interval* 100)
(defvar *key-counters-save-file* nil)

(defstruct key-counters
  (file nil)
  (save-count-down *key-counters-save-interval*)
  (start-time nil)
  (end-time nil)
  (pressed (make-array 1024 :initial-element 0)))

(defun key-counters-up (counters key-code)
  (incf (aref (key-counters-pressed counters) key-code) 1)
  (symbol-macrolet ((scd (key-counters-save-count-down counters)))
    (decf scd 1)
    (when (<= scd 0)
      (setf scd *key-counters-save-interval*)
      (key-counters-save counters))))

(defun key-counters-print (counters strm)
  (loop for line in (sort
                      (loop for cnt across (key-counters-pressed counters)
                            for code from 0
                            when (< 0 cnt)
                            collect (format nil "~A (~A) = ~A~%" (gethash code imv:*key-v2n-table*) code cnt))
                      #'string<)
        do (princ line strm)))

(defun key-counters-save (counters)
  (with-open-file (s (key-counters-file counters) :direction :output :if-exists :supersede)
    (key-counters-print counters s)))

(defun key-counters-load (counters)
  (with-open-file (s (key-counters-file counters) :direction :input)
    (let ((pressed (key-counters-pressed counters)))
      (loop for line = (read-line s nil 'eof)
            until (eq line 'eof)
            do (cl-ppcre:register-groups-bind
                 ((#'parse-integer code) (#'parse-integer cnt))
                 ("\\((\\d+)\\) *= *(\\d+)" line)
                 (setf (aref pressed code) cnt))))))

(when *key-counters-save-file*
  (defvar *key-counters* (make-key-counters :file *key-counters-save-file*))

  (when (probe-file (key-counters-file *key-counters*))
    (key-counters-load *key-counters*))

  (defmethod udev-send-event :after ((udev udev-base) type code value)
    (when (and (= type imv:ev-key)
               (= value imv:value-down))
      (key-counters-up *key-counters* code))))

; }}}

; main {{{

(run "notify-send" "iron-maiden" "reloaded")

(iron-maiden

  (define-keyboard "HHKB"
                   :path #P"/dev/input/by-id/usb-Topre_Corporation_HHKB_Professional-event-kbd")
  (define-keyboard "Pedal3"
                   :path #P"/dev/input/by-id/usb-RDing_FootSwitch3F1.-event-mouse")
  (define-keyboard "PokerProS"
                   :path #P"/dev/input/by-id/usb-Heng_Yu_Technology_Poker_Pro_S-event-kbd")
  (define-keyboard "Poker2"
                   :path #P"/dev/input/by-id/usb-Heng_Yu_Technology_Poker_II-event-kbd")
  (define-keyboard "Poker2Sub"
                   :path #P"/dev/input/by-id/usb-Heng_Yu_Technology_Poker_II-event-if01")
  (define-keyboard "Race"
                   :path #P"/dev/input/by-id/usb-04d9_USB_Keyboard-event-kbd")
  (define-keyboard "Thinkpad"
                   :path #P"/dev/input/by-id/usb-Lite-On_Technology_Corp._ThinkPad_USB_Keyboard_with_TrackPoint-event-kbd")
  (define-keyboard "ThinkpadBT"
                   :name "ThinkPad Compact Bluetooth Keyboard with TrackPoint"
                   :delay t)
  (define-keyboard "Kinesis"
                   :path #P"/dev/input/by-id/usb-05f3_0007-event-kbd")
  (define-keyboard "SinglePedal"
                   :path #P"/dev/input/by-id/usb-MKEYBOARD_3011-event-mouse")
  (define-keyboard "AnkerBTKB"
                   :name "Anker Bluetooth Keyboard")
  (define-keyboard "LaserKB"
                   :name "ELECOM TK-PBL042")
  (define-keyboard "Internal"
                   :name "AT Translated Set 2 keyboard")

  (define-mouse "PowerMate"
                :path #P"/dev/input/by-id/usb-Griffin_Technology__Inc._Griffin_PowerMate-event-if00")
  (define-mouse "SBT"
                :path #P"/dev/input/by-id/usb-Kensington_Kensington_Slimblade_Trackball-event-mouse")
  (define-mouse "ExpertMouse"
                :path #P"/dev/input/by-id/usb-Kensington_Kensington_Expert_Mouse-event-mouse")
  (define-mouse "RingMouse"
                :name "Genius Ring Mouse nRF")

  ; safety
  (let ((ring (imrb:make-ring-buffer 10)))
    (define-processor
      (for-keyboard
        (when-key-down
          (when (= code key-esc)
            (imrb:put-to-buffer ring msec)
            (when-let (next (imrb:next-buffer ring))
              (when (< (- msec next) 2000)
                (imrb:clear-all-buffer ring)
                (sb-ext:exit))))))))

  ; pp
  (define-processor
    (when (and *output-mode* (or *output-mode-put-syn* (/= event-type imv:ev-syn)))
      (device-debug-out dev (pp nil ev)))
    (for-keyboard
      (key-map-to-action
        "<leftalt><leftalt><leftalt>"
        (toggle-output-mode))))

  ; ignore
  (define-processor
    (for-device "PowerMate"
      (next-event)))


  ; フットペダルを特殊キーの入力用に使用する
  (define-processor
    (for-device "SinglePedal"
      (when-key-down
        (setf *foot-pedal-pressed* t))
      (when-key-up
        (setf *foot-pedal-pressed* nil))
      (next-event))
    (when *foot-pedal-pressed*
      (for-keyboard
        (key-map-to-action "w" (imx:warp-pointer 0 -5))
        (key-map-to-action "a" (imx:warp-pointer -5 0))
        (key-map-to-action "s" (imx:warp-pointer 0 5))
        (key-map-to-action "d" (imx:warp-pointer 5 0))
        (change key-1 key-f1)
        (change key-2 key-f2)
        (change key-3 key-f3)
        (change key-4 key-f4)
        (change key-5 key-f5)
        (change key-6 key-f6)
        (change key-7 key-f7)
        (change key-8 key-f8)
        (change key-9 key-f9)
        (change key-0 key-f10)
        (change key-j key-down)
        (change key-k key-up)
        (change key-h key-left)
        (change key-l key-right))))
  (define-processor
    (when *foot-pedal-pressed*
      (pass)
      (next-event)))

  ; Kinesis は特殊なため先に処理する
  (define-processor
    (for-device "Kinesis"

      (key-map-to-key "<rightmeta>" "<A-S-enter>")
      (key-map-to-action "<leftalt>" (toggle-output-mode))
      (key-map-to-action "<pagedown>" (toggle-xmonad-pad "zsh"))
      (key-map-to-action "<pageup>" (toggle-xmonad-pad "shell"))

      (x-and-y key-space nil key-leftshift)
      (x-and-y key-leftctrl key-esc key-leftalt)
      (x-and-y key-rightctrl key-esc key-leftalt)
      (x-and-y key-enter nil key-leftalt)
      (x-and-y key-delete key-enter key-leftalt)
      (x-and-y key-backspace key-space key-leftshift)
      (x-and-y key-apostrophe nil key-leftctrl)
      (x-and-y key-semicolon nil key-leftctrl)

      (key-map-to-key "<f1>" "<A-1>")
      (key-map-to-key "<f2>" "<A-2>")
      (key-map-to-key "<f3>" "<A-3>")
      (key-map-to-key "<f4>" "<A-4>")
      (key-map-to-key "<f5>" "<A-5>")
      (key-map-to-key "<f6>" "<A-6>")
      (key-map-to-key "<f7>" "<A-7>")
      (key-map-to-key "<f8>" "<A-8>")

      (key-map-to-action "<F9>" (remocon "dvi"))
      (key-map-to-action "<F10>" (remocon "hdmi1"))
      (key-map-to-action "<F11>" (remocon "hdmi2"))
      (key-map-to-action "<F12>" (remocon "mute"))

      (key-map-to-key "<up>" "<S-9>")
      (key-map-to-key "<down>" "<S-0>")

      (change key-capslock key-leftctrl)))

  ; ネタ
  ; (define-processor
  ;   (for-device "ExpertMouse"
  ;     (reverse-pointer)))

  ; change keys
  (define-processor
    ; TypeMatrix は特殊なため先に処理する
    (for-device "TypeMatrix"
      (change key-backspace key-esc)
      (change key-capslock key-leftalt)
      (change key-leftmeta key-leftalt)
      (change key-compose key-leftalt)
      (change key-rightctrl key-leftalt)
      (change key-left key-leftalt)
      (change key-leftshift key-leftctrl))

    (for-device ("PokerProS" "Poker2" "AnkerBTKB" "Thinkpad" "Internal" "Race")
      (x-and-y key-space nil key-leftshift)
      (x-and-y key-semicolon nil key-leftctrl)
      (x-and-y key-apostrophe nil key-leftctrl))

    ; 全キーボード
    (for-keyboard
      (change key-leftmeta key-leftalt)
      (change key-capslock key-leftctrl))

    ; 右上のキーを以下のようにする
    ; [equal][backslash]
    ;   [r-brace][grave]
    ;        [e n t e r]

    ; 対象配列
    ; 左      右
    ; `       Backspace
    ;         Backslash
    (for-device "Race"
      (change key-grave key-esc)
      (change key-backspace key-backslash)
      (change key-backslash key-grave))

    ; 対象配列
    ; 右
    ; Backspace
    ; Backslash
    (for-device ("PokerProS" "Poker2" "AnkerBTKB" "Thinkpad" "Internal")
      (change key-backspace key-backslash)
      (change key-backslash key-grave))

    ; for application
    (for-keyboard
      (when-key-press
        (for-window-class "sxiv"
          (change key-k key-p)
          (change key-j key-n)))))

  ; key-map
  (define-processor
    (for-keyboard
      (key-map-to-action "<home>" (imx:warp-pointer 5 0))
      (key-map-to-action "<end>" (imx:warp-pointer -5 0))
      (key-map-to-action "<pageup>" (imx:warp-pointer 0 5))
      (key-map-to-action "<pagedown>" (imx:warp-pointer 0 -5))

      ; (for-window-class ("Navigator" "Dialog")
      ;   (key-map "<C-h>" "<backspace>"))
      (for-window-class "chromium"
        (key-map "<C-S-w>" "<C-w>")
        (key-map "<C-w>" "<backspace>")
        (key-map "<C-h>" "<backspace>"))))

  ; test
  ; (define-processor
  ;   (for-device "RingMouse"
  ;     (/ 0 0)))

  ; rhythm
  (define-processor
    (for-keyboard
      (rhythm-map
        key-leftshift '(1 1 1)
        (press-key im "<A-C-f1>"))
      (rhythm-map
        key-rightshift '(1 1 1)
        (press-key im "<A-C-f7>"))
      (rhythm-map
        key-leftctrl '(100 30)
        (slock)))))
