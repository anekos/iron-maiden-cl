
(load "~/quicklisp/setup.lisp")
(load "iron-maiden.cl")

(ql:quickload :cl-test-more)

(in-package :cl-test-more)


(defmacro isp (except actual)
  `(is ,except
       ,actual
       :test #'equalp))



(diag "key-pair-list-from-text")
(is (im-key::key-pair-list-from-text "<C-S-a>b<f1>")
    (list (cons imv:key-a (list imv:key-leftctrl
                                imv:key-leftshift))
          (cons imv:key-b nil)
          (cons imv:key-f1 nil)))

(diag "im-common:$")
(is (macroexpand '(im-common:$ obj foo)) '(foo obj))
(is (macroexpand '(im-common:$ obj foo bar)) '(bar (foo obj)))
(is (macroexpand '(im-common:$ obj foo bar (baz _ arg1 arg2))) '(baz (bar (foo obj)) arg1 arg2))
(is (macroexpand '(im-common:$ obj foo bar (baz _ arg1 _ arg2))) '(baz (bar (foo obj)) arg1 (bar (foo obj)) arg2))

(is (im-common:group '(1 2 3 4 5 6) 2) '((1 2) (3 4) (5 6)))
(is (im-common:group '(1 2 3 4 5 6) 3) '((1 2 3) (4 5 6)))


(diag "ring-buffer")
(let ((rb (im-ring-buffer:make-ring-buffer 4)))
  (im-ring-buffer:put-to-buffer rb 'a)
  (im-ring-buffer:put-to-buffer rb 'b)

  (isp (im-ring-buffer:get-straight rb)
       (vector nil nil 'a 'b))

  (im-ring-buffer:put-to-buffer rb 'c)
  (im-ring-buffer:put-to-buffer rb 'd)

  (isp (im-ring-buffer:get-straight rb)
       (vector 'a 'b 'c 'd))

  (im-ring-buffer:put-to-buffer rb 'e)

  (isp (im-ring-buffer:get-straight rb)
       (vector 'b 'c 'd 'e))

  (im-ring-buffer:put-to-buffer rb 'f)
  (im-ring-buffer:put-to-buffer rb 'g)

  (isp (im-ring-buffer:get-straight rb)
       (vector 'd 'e 'f 'g)))


(diag "key-map")
(let ((m1 (im-key-map:make-key-map "abc" (lambda () nil)))
      (kh (im-key-history:make-key-history)))
  ; zabc
  (im-key-history:push-state kh imv:key-z nil)
  (im-key-history:push-state kh imv:key-a nil)
  (im-key-history:push-state kh imv:key-b nil)
  (im-key-history:push-state kh imv:key-c nil)
  ; test 1
  (is (im-key-map:test-history m1 kh) t)
  ; x
  (im-key-history:push-state kh imv:key-x nil)
  ; test 2
  (is (im-key-map:test-history m1 kh) nil))

(let ((m1 (im-key-map:make-key-map "<S-a>" (lambda () nil)))
      (kh (im-key-history:make-key-history)))
  ; <S-a>
  (im-key-history:push-state kh imv:key-a '(:shift))
  ; test
  (is (im-key-map:test-history m1 kh) t))
