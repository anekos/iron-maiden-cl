# What is iron-maiden-cl?
iron-maiden-cl is a utility for keyboard customization.
(on Linux)

iron-maiden-cl
- enable one shot modifier easily
- works without X-window system
- use different configurations for each keyboard.

# Installation
## Dependencies on Ubuntu
```
sudo apt-get install git sbcl make gcc wget inotify-tools
```
## Installing iron-maiden-cl
```
git clone https://bitbucket.org/anekos/iron-maiden-cl.git
cd iron-maiden-cl
make
```
If there is a binary file named **iron-maiden**, You've successfully built iron-maiden-cl.

# How to use iron-maiden-cl

## How to run iron-maiden-cl
```
sudo ./iron-maiden THE_CONFIG_FILE
```

## Sample configuration (please save this as [a filename like] rc.cl)
```
(in-package :im-user)

(defmacro x-and-y (original-key change-key modifier-key)
  (with-gensyms (pressing any-key-pressed)
    (eval `(defvar ,pressing nil))
    (eval `(defvar ,any-key-pressed nil))
    `(when-key-press
       (if (= code ,original-key)
         (equal-case
           value
           (imv:value-down
             (setf ,pressing t)
             (next-event))
           (imv:value-repeat
             (next-event))
           (imv:value-up
             (if ,any-key-pressed
               (progn
                 (send-event im imv:ev-key ,modifier-key imv:value-up)
                 (setf ,any-key-pressed nil))
               (progn
                 (let ((send-key (or ,change-key ,original-key)))
                   (send-event im imv:ev-key send-key imv:value-down)
                   (send-event im imv:ev-key send-key imv:value-up))))
             (setf ,pressing nil)
             (next-event)))
         (when (and (= value imv:value-down) ,pressing (not ,any-key-pressed))
           (send-event im imv:ev-key ,modifier-key imv:value-down)
           (setf ,any-key-pressed t))))))

(iron-maiden

  (define-keyboard "lets"
                   :path #P"/dev/input/by-path/platform-i8042-serio-0-event-kbd")
  ; thinkpad-usb-keyboard
  (define-keyboard "thinkpad-usb-keyboard"
                   :path #P"/dev/input/by-path/pci-0000:00:1d.2-usb-0:2:1.0-event-kbd")

  (define-processor
    (for-device "lets"

      (x-and-y key-space nil key-leftctrl)
      (change key-capslock key-enter)
 
)))
```