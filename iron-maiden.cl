#-sbcl (error "This is SBCL specific")
; vim: set lispwords+=tap,with-user-device,with-mutex,catch,for-device,with-gensyms,with-display,block,when-let*,when-let :

; quick lisp {{{

(ql:quickload :clx)
(ql:quickload :cl-ppcre)
(ql:quickload :cl-annot)
(ql:quickload :alexandria)
(ql:quickload :cl-fad)
(ql:quickload :split-sequence)

(cl-annot::enable-annot-syntax)

; }}}

; package iron-maiden-external-command {{{

(defpackage iron-maiden-external-command
  (:use :common-lisp :alexandria)
  (:nicknames :im-external-command :imec))
(in-package :iron-maiden-external-command)


@export (defvar *user* nil)
@export (defvar *path* nil)
@export (defvar *inotifywait-command* "inotifywait")

@export
(defun make-path-list-from-shell (shell-command user-name)
  (mapcar
    (lambda (it)
      (make-pathname :directory
                     `(:absolute
                        ; FIXME
                        ,@(cdr (split-sequence:split-sequence #\/ it)))))
    (split-sequence:split-sequence
      #\:
      (with-output-to-string (strm)
        (sb-ext:run-program "/bin/sudo" `("-i" "-u" ,user-name ,shell-command "-c" "echo $PATH") :output strm)))))

@export
(defun run (cmd args &key (user *user*) (wait nil) (output t) (error t))
  "Run external program
  @param user User name
  @param wait Run synchronous"
  (if user
    (sb-ext:run-program
      "/bin/sudo"
      `("--preserve-env", "-u" ,user ,cmd ,@(alexandria:flatten args))
      :wait wait
      :output output
      :error error
      :if-error-exists t
      :search t)
    (sb-ext:run-program
      cmd
      args
      :wait wait
      :output output
      :error error
      :if-error-exists t
      :search t)))

@export
(defun inotifywait (path &key (events "create") (sleep-if-no-command nil) (delay nil))
  (labels
    ((exec ()
       (run *inotifywait-command*
            (list "-e" events path)
            :wait t
            :output nil)
       (when delay
         (sleep delay))))
    (if sleep-if-no-command
      (if *inotifywait-command*
        (exec)
        (sleep sleep-if-no-command))
      (exec))))

; }}}

; package iron-maiden-c {{{

(defpackage iron-maiden-c
  (:use :common-lisp :sb-alien :sb-c-call)
  (:nicknames :imc))
(in-package :iron-maiden-c)

(load-shared-object
  (merge-pathnames (make-pathname :name "iron-maiden.so")
                   (truename #P".")))

@export (define-alien-routine "create_uinput_device" void (fd int))
@export (define-alien-routine "setup_uinput_device" void (fd int))
@export (define-alien-routine "send_event" void (fd int) (type int) (code int) (value int))
@export (define-alien-routine "setup_input_device" void (fd int))

; }}}

; package iron-maiden-value {{{

(defpackage iron-maiden-value
  (:use :common-lisp :cl-ppcre)
  (:nicknames :imv))
(in-package :iron-maiden-value)

; キモーイ！ *.h の定数定義を適当に読むよ！！！ {{{

@export (defvar *key-v2n-table* (make-hash-table))
@export (defvar *key-n2v-table* (make-hash-table))

(defun parse-define (line)
  (labels ((parse-const-value (s)
             (cond
               ((scan "^(0x|\\d)" s)
                (multiple-value-bind (n x?)
                  (regex-replace "0x" s "")
                  (parse-integer n :radix (if x? 16 10))))
               ((scan "^[a-zA-Z_]+$" s)
                (eval (intern (format-name s))))
               ; 横着してる
               (t (register-groups-bind ((#'format-name name) (#'parse-integer value))
                    ("^\\(([a-zA-Z_]+)\\+(\\d+)\\)$" s)
                    (+ (eval (intern name)) value)))
               (t nil)))
           (format-name (s)
             (regex-replace-all "_" s "-"))
           (split-name (s)
             (register-groups-bind
               (klass (#'format-name name))
               ("^([^-]+)-(.+)$" s)
               (values klass (string-downcase name)))))
    (when (scan "^#define " line)
      (register-groups-bind
        ((#'format-name name) value)
        ("^#define (\\S+)\\s+(\\S+)" line)
        (let ((sym (intern name))
              (value (parse-const-value value)))
          (multiple-value-bind (klass name) (split-name name)
            (when (equal klass "KEY")
              (setf (gethash name *key-n2v-table*) value)
              (setf (gethash value *key-v2n-table*) name)))
          (eval `(defvar ,sym ,value))
          (export sym))))))

(defun select-file (path &rest rest-path)
  (if (probe-file path)
    path
    (apply #'select-file rest-path)))

(with-open-file (s (select-file
                     #P"/usr/include/linux/input-event-codes.h"
                     #P"/usr/include/linux/input.h"))
  (loop for line = (read-line s nil) while line
        do (parse-define line)))

@export (defvar value-down 1)
@export (defvar value-up 0)
@export (defvar value-repeat 2)

@export
(defvar event-types '(ev-syn ev-key ev-rel ev-abs ev-msc ev-sw ev-led ev-snd ev-rep ev-ff ev-pwr ev-max ev-cnt)) ; ev-ff-status


; }}}

; }}}

; package iron-maiden-common {{{

(defpackage iron-maiden-common
  (:use :common-lisp :sb-alien :sb-c-call :alexandria :sb-thread)
  (:nicknames :im-common))
(in-package :iron-maiden-common)

(defvar *debug-output-mutex* (make-mutex :name "iron-maiden-debug-output"))

@export
(defmacro in-debug-out (&rest body)
  `(with-mutex (*debug-output-mutex*)
     ,@body))

@export
(defun debug-out (name text &rest args)
  (in-debug-out
    (format t "[~A] " (or name "Info"))
    (apply #'format `(t ,text ,@args))
    (format t "~%")))

(define-symbol-macro it (intern "IT"))

@export
(defmacro tap (expr &rest body)
  `(let ((,it ,expr))
     ,@body
     ,it))

@export
(defmacro -> (var name &rest names)
  (if names
    `(-> (slot ,var ,name) ,@names)
    `(slot ,var ,name)))

@export
(defmacro --> (var name &rest names)
  (if names
    `(--> (slot-value ,var ,name) ,@names)
    `(slot-value ,var ,name)))

@export
(defmacro pushf (var value)
  `(setf ,var (append ,var (list ,value))))

@export
(defun put-error (e)
  (debug-out "Error" "~A" e))

@export
(defmacro equal-case (key-form &rest normal-clauses)
  (with-gensyms (key)
    `(let ((,key ,key-form))
       (cond
         ,@(mapcar
             (lambda (clause)
               (destructuring-bind
                 (clause-key . clause-form) clause
                 (if (equal clause-key 'otherwise)
                   `(t ,@clause-form)
                   `((equal ,key ,clause-key) ,@clause-form))))
             normal-clauses)))))

@export
(defmacro $ (expr &rest calls)
  (if calls
    (destructuring-bind
      (head . tail) calls
      (typecase head
        (symbol  `($ (,head ,expr) ,@tail))
        (list (let ((ph (intern "_")))
                (if (find ph head)
                  `($ ,(mapcar
                         (lambda (elem)
                           (if (equal elem ph) expr elem))
                         head)
                      ,@tail)
                  `($ ,expr ,@head))))))
    expr))

@export
(defun group (source n)
  (when source
    (cons
      (subseq source 0 n)
      (group (subseq source n) n))))

; class pretty-print {{{

@export
(defclass pretty-print ()
  ())

@export (defgeneric pp (strm self))

(defmethod pp (strm (pp pretty-print))
  (format strm "~A" pp))

; }}}

; }}}

; package iron-maiden-ring-buffer {{{

(defpackage iron-maiden-ring-buffer
  (:use :common-lisp :sb-alien :sb-c-call :alexandria)
  (:nicknames :im-ring-buffer :imrb))
(in-package :iron-maiden-ring-buffer)

(defclass ring-buffer ()
  ((buffer-size :initarg :size)
   (buffer-array :initarg :array)
   (pointer :initform nil)))

@export (defgeneric current-buffer (rb))
@export (defgeneric previous-buffer (rb))
@export (defgeneric next-buffer (rb))
@export (defgeneric put-to-buffer (rb obj))
@export (defgeneric clear-all-buffer (rb))
@export (defgeneric get-straight (rb))
(defgeneric current-pointer (rb))
(defgeneric previous-pointer (rb))
(defgeneric next-pointer (rb))


@export
(defun make-ring-buffer (size)
  (make-instance 'ring-buffer
                 :size size
                 :array (make-array size :initial-element nil)))

(defmethod current-pointer ((rb ring-buffer))
  (slot-value rb 'pointer))

(defmethod previous-pointer ((rb ring-buffer))
  (when-let (cpt (current-pointer rb))
    (let ((pt (1- cpt)))
      (if (< pt 0)
        (1- (slot-value rb 'buffer-size))
        pt))))

(defmethod next-pointer ((rb ring-buffer))
  (when-let (cpt (current-pointer rb))
    (let ((pt (1+ cpt)))
      (if (<= (slot-value rb 'buffer-size) pt)
        0
        pt))))

(defmethod put-to-buffer ((rb ring-buffer) obj)
  (setf (slot-value rb 'pointer)
        (or (next-pointer rb)
            0))
  (setf (aref (slot-value rb 'buffer-array)
              (current-pointer rb))
        obj))

(defmethod current-buffer ((rb ring-buffer))
  (when-let (pt (current-pointer rb))
    (aref (slot-value rb 'buffer-array)
          pt)))

(defmethod previous-buffer ((rb ring-buffer))
  (when-let (pt (previous-pointer rb))
    (aref (slot-value rb 'buffer-array)
          pt)))

(defmethod next-buffer ((rb ring-buffer))
  (when-let (pt (next-pointer rb))
    (aref (slot-value rb 'buffer-array)
          pt)))

(defmethod clear-all-buffer ((rb ring-buffer))
  (setf (slot-value rb 'buffer-array)
        (make-array (slot-value rb 'buffer-size) :initial-element nil)))

(defmethod get-straight ((rb ring-buffer))
  (when-let ((pt (current-pointer rb))
             (buf (slot-value rb 'buffer-array)))
    (concatenate 'vector
                 (subseq buf (1+ pt))
                 (subseq buf 0 (1+ pt)))))

; }}}

; package iron-maiden-event {{{

(defpackage iron-maiden-event
  (:use :iron-maiden-common
        :common-lisp :sb-alien :sb-c-call)
  (:nicknames :im-event))
(in-package :iron-maiden-event)

; types {{{

(define-alien-type
  nil
  (struct timeval
          (sec (unsigned 64))
          (usec (unsigned 64))))

(define-alien-type
  nil
  (struct input-event-struct
          (time (struct timeval))
          (type (unsigned 16))
          (code (unsigned 16))
          (value (integer 32))))

(export '(time type code value))

; }}}

; class input-event-raw {{{

@export
(defclass input-event-raw (pretty-print)
  ((data :initarg :data)))

@export (defgeneric raw-data-of (ev))
@export (defgeneric raw-time-of (ev))
@export (defgeneric raw-sec-of (ev))
@export (defgeneric raw-usec-of (ev))
@export (defgeneric raw-code-of (ev))
@export (defgeneric raw-type-of (ev))
@export (defgeneric raw-value-of (ev))
@export (defgeneric raw-stringify-kind (ev))
@export (defgeneric raw-stringify-value (ev))
@export (defgeneric raw-send-event (udev ev &key type code value))


(defmethod raw-data-of ((ev input-event-raw))
  (slot-value ev 'data))

(defmethod pp (strm (ev input-event-raw))
  (format strm "<input-event time=~A~20T type=~A code=~3,'0D~55T value=~A>~%"
          (raw-time-of ev)
          (raw-stringify-kind ev)
          (raw-code-of ev)
          (raw-stringify-value ev)))

(macrolet
  ((defaccessor (name &rest path)
     (let ((ev (gensym "ev")))
       `(defmethod ,name ((,ev input-event-raw))
          (-> (raw-data-of ,ev) ,@path)))))
  (defaccessor raw-sec-of 'time 'sec)
  (defaccessor raw-usec-of 'time 'usec)
  (defaccessor raw-code-of 'code)
  (defaccessor raw-type-of 'type)
  (defaccessor raw-value-of 'value))

(defmethod raw-time-of ((ev input-event-raw))
  (let ((data (raw-data-of ev)))
    (cons
      (-> data 'time 'sec)
      (-> data 'time 'usec))))

(defmethod raw-send-event (udev (ev input-event-raw) &key type code value)
  (let ((data (raw-data-of ev)))
    (imc:send-event udev
      (or type (-> data 'type))
      (or code (-> data 'code))
      (or value (-> data 'value)))))

(labels ((find-name (v lst)
           (find-if (lambda (x) (= (eval x) v)) lst)))
  (defmethod raw-stringify-kind ((ev input-event-raw))
    (format nil "~{~A(~A)~}"
            (let ((tv (-> (raw-data-of ev) 'type)))
              (list (find-name tv imv:event-types) tv))))
  (defmethod raw-stringify-value ((ev input-event-raw))
    (format nil "~{~A(~A)~}"
      (let ((tv (-> (raw-data-of ev) 'value)))
        (list (find-name tv '(imv:value-down imv:value-up imv:value-repeat)) tv)))))

; }}}

; class input-event-base {{{

@export
(defclass input-event-base (input-event-raw)
  ((raw :initarg :raw
        :accessor raw-of)))

@export (defgeneric modify-event-value (ev data-name modifier))
@export (defgeneric update-event-value (ev data-name new-value))

(defmethod pp (strm (ev input-event-base))
  (format strm "<input-event time=~A~20T type=~A code=~3,'0D~55T value=~A>"
          (raw-time-of ev)
          (raw-stringify-kind ev)
          (raw-code-of ev)
          (raw-stringify-value ev)))

(defmethod raw-data-of ((ev input-event-base))
  (raw-data-of (slot-value ev 'raw)))

@export
(defun read-event (handle klass)
  (let ((buf (make-alien (struct input-event-struct))))
    (sb-posix:read handle buf 24)
    (make-instance klass
                   :raw (make-instance 'input-event-raw :data buf))))

(defmethod modify-event-value ((ev input-event-base) data-name modifier)
  (symbol-macrolet ((data (slot (raw-data-of ev) data-name)))
    (setf data (funcall modifier data))))

(defmethod update-event-value ((ev input-event-base) data-name new-value)
  (setf (slot (raw-data-of ev) data-name) new-value))

; }}}

; class mouse-input-event {{{

@export
(defclass mouse-input-event (input-event-base)
  ())

@export (defgeneric mouse-button-of (mev))

(defmethod mouse-button-of ((mev mouse-input-event))
  (logand #xff (raw-code-of mev)))

(defmethod pp (strm (mev mouse-input-event))
  (let ((kind (raw-type-of mev)))
    (cond
      ((= kind imv:ev-rel)
       (format strm "<mouse-event time=~A~20T axis=~A(~A) type=~A value=~A>"
                (raw-time-of mev)
                (case (raw-code-of mev)
                  (0 "x")
                  (1 "y"))
                (raw-code-of mev)
                (raw-stringify-kind mev)
                (raw-value-of mev)))
      (t
        (format strm "<mouse-event time=~A~20T button=~A(~A) type=~A value=~A>"
                (raw-time-of mev)
                (mouse-button-of mev)
                (raw-code-of mev)
                (raw-stringify-kind mev)
                (raw-stringify-value mev))))))

; }}}

; class keyboard-input-event {{{

@export
(defclass keyboard-input-event (input-event-base)
  ())

@export (defgeneric raw-key-code-of (kev))
@export (defgeneric raw-stringify-code (ev))

(defmethod raw-key-code-of ((kev keyboard-input-event))
  (raw-code-of kev))

(defmethod raw-stringify-code ((ev input-event-raw))
  (format nil "~{~A(~A)~}"
          (let ((code (-> (raw-data-of ev) 'code)))
            (list (gethash code imv:*key-v2n-table* code) code))))

(defmethod pp (strm (kev keyboard-input-event))
  (let* ((code (raw-code-of kev))
         (pcode (if (= (raw-type-of kev) imv:ev-key)
                  (raw-stringify-code kev)
                  code)))
    (format strm "<keyboard-event time=~A~43T type=~A key=~A~80T value=~A>"
            (raw-time-of kev)
            (raw-stringify-kind kev)
            pcode
            (raw-stringify-value kev))))
; }}}

; class joystick-input-event {{{

@export
(defclass joystick-input-event (input-event-base)
  ())

@export (defgeneric joystick-button-of (mev))

(defmethod joystick-button-of ((mev joystick-input-event))
  (logand #xff (raw-code-of mev)))

(defmethod pp (strm (mev joystick-input-event))
  (let ((kind (raw-type-of mev)))
    (cond
      ((= kind imv:ev-abs)
       (format strm "<joystick-event time=~A~20T axis=~A(~A) type=~A value=~A>"
                (raw-time-of mev)
                (case (raw-code-of mev)
                  (0 "x")
                  (1 "y"))
                (raw-code-of mev)
                (raw-stringify-kind mev)
                (raw-value-of mev)))
      (t
        (format strm "<joystick-event time=~A~20T button=~A(~A) type=~A value=~A>"
                (raw-time-of mev)
                (joystick-button-of mev)
                (raw-code-of mev)
                (raw-stringify-kind mev)
                (raw-stringify-value mev))))))

; }}}

; }}}

; package iron-maiden-finder {{{

(defpackage iron-maiden-finder
  (:use :common-lisp :alexandria)
  (:nicknames :im-finder))
(in-package :iron-maiden-finder)

; class device-finder-base {{{

@export
(defclass device-finder-base ()
  ())

@export (defgeneric find-device (finder))

(defmethod find-device (device-finder-base)
  nil)

; }}}

; class device-finder-by-path {{{

@export
(defclass device-finder-by-path (device-finder-base)
  ((path :initarg :path)))

(defmethod find-device ((finder device-finder-by-path))
  (slot-value finder 'path))

; }}}

; class device-finder-by-name {{{

(defun get-device-info-hash-table (lines)
  (alexandria:alist-hash-table
    (loop for line in lines
          collect (cl-ppcre:register-groups-bind
                    (name value)
                    ("[A-Z]: ([^=]+)=\"?([^\"]+)" line)
                    (cons name value)))
    :test #'equal))

(defun get-device-info-list (key)
  (let* ((lines (with-open-file (s "/proc/bus/input/devices")
                  (loop with l
                        do (setf l (read-line s nil))
                        while l
                        collect l)))
         (splited (split-sequence:split-sequence "" lines :test #'equal)))
    (alist-hash-table
      (loop with name-and-event = nil
            for info in (mapcar #'get-device-info-hash-table splited)
            do (setf name-and-event
                     (when-let ((key-value (gethash key info))
                                           (ev-line (gethash "Handlers" info)))
                       (cons key-value
                             (cl-ppcre:scan-to-strings "event\\d+" ev-line))))
            when name-and-event collect name-and-event)
      :test #'equal)))

@export
(defclass device-finder-by-name (device-finder-base)
  ((name :initarg :name)))

(defmethod find-device ((finder device-finder-by-name))
  (let ((dev-table (get-device-info-list "Name")))
    (when-let (ev (gethash (slot-value finder 'name) dev-table))
      (concatenate 'string "/dev/input/" ev))))

; }}}

; utils {{{

@export
(defun make-finder (finder-type key)
  (case finder-type
    (:path (make-instance 'device-finder-by-path :path key))
    (:name (make-instance 'device-finder-by-name :name key))))

; }}}

; }}}

; package iron-maiden-state {{{

(defpackage iron-maiden-state
  (:use :iron-maiden-event
        :common-lisp :alexandria)
  (:nicknames :im-state))
(in-package :iron-maiden-state)


(defvar
  modifiers-keys
  (alist-hash-table (list (cons :ctrl
                                (list imv:key-leftctrl imv:key-rightctrl))
                          (cons :alt
                                (list imv:key-leftalt imv:key-rightalt))
                          (cons :meta
                                (list imv:key-leftmeta imv:key-rightmeta))
                          (cons :shift
                                (list imv:key-leftshift imv:key-rightshift)))))
(defvar
  modifiers-key-values
  (apply
    #'concatenate
    (cons 'list (hash-table-values modifiers-keys))))

@export (defgeneric state-on-event (st ev))
@export (defgeneric state-on-event-3 (st type code value))

(defconstant keyboard-state-array-size #x1000)

@export
(defclass keyboard-state ()
  ((keys :initform (make-array keyboard-state-array-size
                               :initial-element 0))
   (modifiers :initform (alist-hash-table '((:ctrl . nil)
                                            (:alt . nil)
                                            (:shift . nil)
                                            (:meta . nil))))))

@export (defgeneric on-down (st code))
@export (defgeneric on-up (st code))
@export (defgeneric update-modifiers (st))
@export (defgeneric current-modifires (st))
@export (defgeneric modifiers-equal (st modifiers))
@export (defgeneric current-key-set (st))
@export (defgeneric reset-keys (st))

(defmethod state-on-event ((st keyboard-state) (ev input-event-base))
  (state-on-event-3 st
                    (raw-type-of ev)
                    (raw-value-of ev)
                    (raw-code-of ev)))

(defmethod state-on-event-3 ((st keyboard-state) type code value)
  (block exit
    (when (= imv:ev-key type)
      (cond
        ((= value imv:value-down) (on-down st code))
        ((= value imv:value-up) (on-up st code))
        (t (return-from exit)))
      (when (find code modifiers-key-values)
        (update-modifiers st)))))

(defmethod on-down ((st keyboard-state) code)
  (incf (aref (slot-value st 'keys) code)))

(defmethod on-up ((st keyboard-state) code)
  (when (> 0
           (decf (aref (slot-value st 'keys) code)))
    (setf (aref (slot-value st 'keys) code) 0)
    (warn (format nil "Code ~d has been fixed to 0" code))))

(defmethod update-modifiers ((st keyboard-state))
  (let ((keys (slot-value st 'keys)))
    (setf (slot-value st 'modifiers)
          (alist-hash-table
            (loop for name being the hash-keys in modifiers-keys using (hash-value value)
                  collect (cons
                            name
                            (loop for key-code in value
                                  when (< 0 (aref keys key-code))
                                  collect key-code)))))))

(defmethod current-modifires ((st keyboard-state))
  (loop for name being the hash-key in (slot-value st 'modifiers) using (hash-value value)
        unless (null value)
        collect name))

(defmethod modifiers-equal ((st keyboard-state) modifiers)
  (not
    (set-exclusive-or
      modifiers
      (current-modifires st))))

(defmethod current-key-set ((st keyboard-state))
  (loop for cnt across (slot-value st 'keys)
        for i from 0
        when (< 0 cnt) collect i))

(defmethod reset-keys ((st keyboard-state))
  (setf (slot-value st 'keys)
        (make-array keyboard-state-array-size)))

; }}}

; package iron-maiden-device {{{

(defpackage iron-maiden-device
  (:use :iron-maiden-common :iron-maiden-finder :iron-maiden-state :iron-maiden-event
        :common-lisp :alexandria)
  (:nicknames :im-device))
(in-package :iron-maiden-device)

(defvar *inotify-wait-delay* 2)

; class device-base {{{

@export
(defclass device-base ()
  ((name :initarg :name
         :accessor name-of)
   (finder :initarg :finder)
   (input-event-class :initform 'input-event-base)
   (grab :initarg :grab
         :initform t)
   (delay :initarg :delay
          :initform nil)
   (handle)))

(export 'name-of)

@export (defgeneric device-path (dev))
@export (defgeneric device-exist? (dev))
@export (defgeneric valid-device? (dev))
@export (defgeneric open-device (dev))
@export (defgeneric read-device-event (dev))
@export (defgeneric run-device-read-loop (dev body))
@export (defgeneric device-debug-out (dev msg &rest args))

(defmethod device-path ((dev device-base))
  (find-device (slot-value dev 'finder)))

(defmethod device-exist? ((dev device-base))
  (when-let (path (find-device (slot-value dev 'finder)))
    (probe-file path)))

(defmethod valid-device? ((dev device-base))
  (or (slot-value dev 'delay) (device-exist? dev)))

(defmethod open-device ((dev device-base))
  (with-slots (grab handle) dev
    (when-let ((path (device-path dev)))
      (setf handle (sb-posix:open path sb-posix:o-rdonly))
      (when grab
        (imc:setup-input-device handle))
      handle)))

(defmethod read-device-event ((dev device-base))
  (with-slots (handle input-event-class) dev
    (read-event handle input-event-class)))


(defmacro desperate-call (f &rest args)
  `(handler-case
    (funcall ,f ,@args)
    (sb-posix:syscall-error (e) (error e))
    (error (e) (put-error e) (sb-ext:exit :abort t))))

(defun wait-for-respawn-device (dev)
  (loop until (device-exist? dev)
        do (im-external-command:inotifywait
             "/dev/input"
             :events "create"
             :sleep-if-no-command 1
             :delay *inotify-wait-delay*)))

(defmethod run-device-read-loop ((dev device-base) body)
  (device-debug-out dev "Device opening")
  (loop do
        (handler-case
          (when-let ((handle (open-device dev)))
            (unwind-protect
              (progn
                (device-debug-out dev "Device opened")
                (loop do
                      (desperate-call body (read-device-event dev))))
              (sb-posix:close handle)))
          (error (e) (put-error e)))
        (device-debug-out dev "Waiting for try to re-open device.")
        (wait-for-respawn-device dev)))

(defmethod device-debug-out ((dev device-base) msg &rest args)
  (apply #'debug-out `(,(format nil "%~A" (name-of dev)) ,msg ,@args)))

; }}}

; class mouse-device {{{

@export
(defclass mouse-device (device-base)
  ((input-event-class :initform 'mouse-input-event)))

; }}}

; class keyboard-device {{{

@export
(defclass keyboard-device (device-base)
  ((input-event-class :initform 'keyboard-input-event)
   (state :initform (make-instance 'keyboard-state))))

(defmethod read-device-event ((dev keyboard-device))
  (tap (call-next-method)
    (when (= imv:ev-key (raw-type-of it))
      (let ((value (raw-value-of it))
            (code (raw-code-of it))
            (state (slot-value dev 'state)))
        (cond
          ((= value imv:value-down) (on-down state code))
          ((= value imv:value-up) (on-up state code)))))))

; }}}

; class joystick-device {{{

@export
(defclass joystick-device (device-base)
  ((input-event-class :initform 'joystick-input-event)))

; }}}

; }}}

; package iron-maiden-udev {{{

(defpackage iron-maiden-udev
  (:use :iron-maiden-common :iron-maiden-event :iron-maiden-state
        :common-lisp)
  (:nicknames :im-udev))
(in-package :iron-maiden-udev)

; class udev-base {{{

@export
(defclass udev-base ()
  ((handle)))

@export (defgeneric udev-open (udev))
@export (defgeneric udev-close (udev))
@export (defgeneric udev-send-event (udev type code value))
@export (defgeneric udev-send-modified-event (udev ev &key type code value))
@export (defgeneric udev-send-syn (udev))

(defmethod udev-open ((udev udev-base))
  (with-slots (handle) udev
    (setf handle (sb-posix:open #P"/dev/uinput" sb-posix:o-wronly))
    (when handle
      (imc:setup-uinput-device handle)
      (imc:create-uinput-device handle)
      handle)))

(defmethod udev-close ((udev udev-base))
  (with-slots (handle) udev
    (sb-posix:close handle)
    (setf handle nil)))

(defmethod udev-send-event ((udev udev-base) type code value)
  (imc:send-event (slot-value udev 'handle)
                  type
                  code
                  value))

(defmethod udev-send-modified-event ((udev udev-base) (ev input-event-raw) &key type code value)
  (let ((data (raw-data-of ev)))
    (udev-send-event udev
      (or type (-> data 'im-event:type))
      (or code (-> data 'im-event:code))
      (or value (-> data 'im-event:value)))))

(defmethod udev-send-syn ((udev udev-base))
  (imc:send-event (slot-value udev 'handle) imv:ev-syn 0 0)
  (imc:send-event (slot-value udev 'handle) imv:ev-msc 0 0))

; }}}

; class udev-keyboard {{{

@export
(defclass udev-keyboard (udev-base)
  ((handle)
   (keyboard-state :initform (make-instance 'keyboard-state))))

@export (defgeneric udev-send-event-key-press (udev key-codes))

(defmethod udev-send-event ((udev udev-keyboard) type code value)
  (tap (call-next-method)
    (state-on-event-3 (slot-value udev 'keyboard-state) type code value)))

(defmethod udev-send-event-key-press ((udev udev-keyboard) key-codes)
  (mapcar
    (lambda (key-code)
      (udev-send-event udev imv:ev-key key-code imv:value-down)
      (udev-send-syn udev))
    key-codes)
  (mapcar
    (lambda (key-code)
      (udev-send-event udev imv:ev-key key-code imv:value-up)
      (udev-send-syn udev))
    (reverse key-codes)))

; }}}

; class udev-mouse {{{

@export
(defclass udev-mouse (udev-base)
  ((handle)))

; }}}

; udev-multi {{{

@export
(defclass udev-multi (udev-keyboard udev-mouse)
  ((handle)))

; }}}

; }}}

; package iron-maiden-event-processor {{{

(defpackage iron-maiden-event-processor
  (:use :common-lisp)
  (:nicknames :im-processor))
(in-package :iron-maiden-event-processor)

; class event-processor-base {{{
(defclass event-processor-base ()
  ((main)
   (exit-code :initform nil)))

(defgeneric set-event-prosessor-main (ep func))
(defgeneric process-event (ep ev))

; (defmethod set-event-prosessor-main ((ep event-processor-base) (main-func function))
;   (with-slots (main) ep
;     (setf
;       ep
;       (labels
;         (do-next ))
;       (make-co-routine
;         (funcall main-func)))))
; }}}

; }}}

; package iron-maiden-key-history {{{

(defpackage iron-maiden-key-history
  (:use :iron-maiden-common :iron-maiden-value :iron-maiden-ring-buffer
        :common-lisp :alexandria :cl-ppcre)
  (:nicknames :im-key-history))
(in-package :iron-maiden-key-history)

; ( (key . modifierss) .... )
@export
(defclass key-history ()
  ((buffer :initform (im-ring-buffer:make-ring-buffer 1000))))

@export
(defun make-key-history ()
  (make-instance 'key-history))

@export (defgeneric push-state (kh key modifiers))
@export (defgeneric get-straight (kh))

(defmethod push-state ((kh key-history) key modifiers)
  (put-to-buffer
    (slot-value kh 'buffer)
    (cons key modifiers)))

(defmethod get-straight ((kh key-history))
  (im-ring-buffer:get-straight (slot-value kh 'buffer)))


; }}}

; package iron-maiden-key {{{

(defpackage iron-maiden-key
  (:use :iron-maiden-common
        :common-lisp :alexandria :cl-ppcre :iron-maiden-value)
  (:nicknames :im-key))
(in-package :iron-maiden-key)

(defun key-name-to-key-code (name)
  (if (scan "^#\\d+$" name)
    (read-from-string (subseq name 1))
    (eval (intern (string-upcase (concatenate 'string "key-" name)) :iron-maiden-value))))

(defun modifier-char-to-key-code (chr)
  (equal-case chr
    ("C" key-leftctrl)
    ("S" key-leftshift)
    ("A" key-leftalt)
    ("M" key-leftmeta)
    (otherwise (error (format nil "Unknown modifier key character: ~A" (type-of chr))))))

(defun modifier-char-to-symbol (chr)
  (equal-case chr
    ("C" :ctrl)
    ("S" :shift)
    ("A" :alt)
    ("M" :meta)
    (otherwise (error (format nil "Unknown modifier key character: ~A" (type-of chr))))))

@export
(defun key-pair-from-text (text &key symbol-modifiers)
  (let ((from-char (if symbol-modifiers
                     #'modifier-char-to-symbol
                     #'modifier-char-to-key-code)))
    (multiple-value-bind
      (matched inner) (scan-to-strings "^<(.*)>$" text)
      (if matched
        (destructuring-bind (key . modifiers) (reverse (split "-" (aref inner 0)))
          (cons
            (key-name-to-key-code key)
            (mapcar from-char (reverse modifiers))))
        (list (key-name-to-key-code text))))))

@export
(defun key-pair-list-from-text (text &key symbol-modifiers)
  (loop for part in (cl-ppcre:all-matches-as-strings "<[^<]+>|." text)
        collect (key-pair-from-text part :symbol-modifiers symbol-modifiers)))

; }}}

; package iron-maiden-key-map {{{

(defpackage iron-maiden-key-map
  (:use :iron-maiden-common
        :common-lisp :alexandria :cl-ppcre :iron-maiden-value)
  (:nicknames :im-key-map))
(in-package :iron-maiden-key-map)

@export
(defclass key-map ()
  ((pairs :initarg :pairs)
   (action :initarg :action)))

@export (defgeneric make-key-map (keys action))
@export (defgeneric test-history (km kh))

(defmethod make-key-map ((keys string) action)
  (let ((pairs (im-key:key-pair-list-from-text keys)))
    (make-instance 'key-map
                   :pairs pairs
                   :action action)))

(defmethod test-history ((km key-map) (kh im-key-history:key-history))
  (let ((tmp-pairs (reverse (slot-value km 'pairs)))
        (history (reverse (im-key-history:get-straight kh))))
    (loop
      for it across history
      do (progn
           (unless it (return nil))
           (if (equal it (car tmp-pairs))
             (progn
               (setf tmp-pairs (cdr tmp-pairs))
               (when (null tmp-pairs)
                 (return t)))
             (progn
               (return nil)))))))

; }}}

; iron-maiden-x {{{

(defpackage iron-maiden-x
  (:use :common-lisp :alexandria)
  (:nicknames :im-x :imx))
(in-package :iron-maiden-x)

@export (defvar *display-host* "")
@export (defvar *display* nil)

(defvar *first-error* t)

(defvar *cache-current-window-id* nil)
(defvar *cache-current-window* nil)
(defvar *cache-current-window-properties* (make-hash-table))

@export
(defun byte-list-to-string (bytes)
  (let ((pos (position 0 bytes)))
    (map 'string #'code-char (subseq bytes 0 pos))))

@export
(defmacro with-x (&rest body)
  `(handler-case
     (progn ,@body)
     (error (c) (when *first-error*
                  (im-common:put-error c)
                  (setf *first-error* nil)))))

@export
(defmacro with-display (&rest body)
  (with-gensyms (opened)
    `(with-x
       (let ((,opened *display*)
             (*display* (or *display* (xlib:open-display *display-host*))))
         (unwind-protect
           (progn ,@body)
           (unless ,opened
             (xlib:close-display *display*)))))))

@export
(defun get-current-window-id ()
  (or
    *cache-current-window-id*
    (setf
      *cache-current-window-id*
      (with-display
        (let ((rootw (xlib:screen-root (car (xlib:display-roots *display*)))))
          (car (xlib:get-property rootw :_NET_ACTIVE_WINDOW)))))))


@export
(defun get-current-window ()
  (or
    *cache-current-window*
    (setf
      *cache-current-window*
      (with-display
        ; (format t "get-current-window~%")
        (when-let*
          ((current-window-id (get-current-window-id)))
          (when (/= 0 current-window-id)
            (xlib::lookup-window *display* current-window-id)))))))
            ; (find-if
            ;   (op = current-window-id (slot-value _ 'xlib::id))
            ;   (xlib:query-tree rootw))))))


@export
(defun get-current-window-property (name)
  (or
    (gethash name *cache-current-window-properties*)
    (setf
      (gethash name *cache-current-window-properties*)
      (with-display
        (let ((cw (get-current-window)))
          (when cw
            (byte-list-to-string
              (xlib:get-property cw name :type :STRING))))))))


@export
(defun warp-pointer (dx dy)
  (with-display
    (xlib:warp-pointer-relative *display* dx dy)
    (xlib:display-force-output *display*)))

@export
(defun reset-cache ()
  (let ((previous-window-id *cache-current-window-id*))
    (setf *cache-current-window-id* nil)
    (setf *cache-current-window-id*  (get-current-window-id))
    ; (format t "previous-window-id = ~A, *cache-current-window-id* = ~A~%" previous-window-id *cache-current-window-id*)
    (when (not (equal *cache-current-window-id* previous-window-id))
      (setf *cache-current-window* nil)
      (clrhash *cache-current-window-properties*))))

; }}}

; package iron-maiden {{{

(defpackage iron-maiden
  (:use :iron-maiden-common :iron-maiden-value :iron-maiden-device :iron-maiden-event :iron-maiden-udev :iron-maiden-finder :iron-maiden-state :iron-maiden-event
        :sb-thread :common-lisp :alexandria)
  (:nicknames :im))
(in-package :iron-maiden)

@export
(defclass iron-maiden ()
  ((udev-multi :initform (make-instance 'udev-multi))
   (devices :initform nil
            :initarg :devices
            :reader devices-of)
   (processors :initform nil
               :initarg :processors
               :reader processors-of)
   (mutex :initform (make-mutex :name "iron-maiden-main"))))

@export (defgeneric start (im))
@export (defgeneric press-key (im codes))
@export (defgeneric send-event (im type code value &key with-syn))
@export (defgeneric send-modified-event (im ev &key type code value))
@export (defgeneric send-syn (im))
@export (defgeneric keyboard-state (im))
@export (defgeneric valid-devices (im))
@export (defgeneric reset-keys (im))

(defgeneric process-event (im dev ev))

(defmethod start ((im iron-maiden))
  (udev-open (slot-value im 'udev-multi))
  (unwind-protect
    (progn
      (let ((ts (mapcar
                  (lambda (dev)
                    (make-thread
                      (lambda ()
                        (run-device-read-loop
                          dev
                          (lambda (ev)
                            (with-mutex ((slot-value im 'mutex))
                              (iron-maiden-x:reset-cache)
                              (process-event im dev ev)))))))
                  (valid-devices im))))
        (mapcar (lambda (x) (join-thread x)) ts)))
    (udev-close (slot-value im 'udev-multi))))

(defmethod press-key ((im iron-maiden) (text string))
  (let* ((udev (slot-value im 'udev-multi))
         (released-keys
           (loop for pair in (im-key:key-pair-list-from-text text)
                 with release-keys = (set-difference (im-state:current-key-set (keyboard-state im))
                                                     pair)
                 append release-keys
                 do (format t "current: ~A~%" (im-state:current-key-set (keyboard-state im)))
                    (when (consp release-keys)
                      (loop for key in release-keys
                            do (udev-send-event udev imv:ev-key key imv:value-up)
                               (udev-send-syn udev)))
                    (udev-send-event-key-press udev (reverse pair)))))
    (loop for key in released-keys
          do (udev-send-event udev imv:ev-key key imv:value-down)
             (udev-send-syn udev))))

(defmethod send-event ((im iron-maiden) type code value &key with-syn)
  (let ((udev (slot-value im 'udev-multi)))
    (udev-send-event udev type code value)
    (when with-syn
      (udev-send-syn udev))))

(defmethod send-modified-event ((im iron-maiden) (ev input-event-base) &key type code value)
  (udev-send-modified-event (slot-value im 'udev-multi) ev :type type :code code :value value))

(defmethod send-syn ((im iron-maiden))
  (udev-send-syn (slot-value im 'udev-multi)))

(export 'next-event)
(export 'next-processor)
(defmethod process-event ((im iron-maiden) (dev device-base) (ev input-event-base))
  (catch 'next-event
    (loop for processor in (processors-of im)
          do (catch 'next-processor
               (funcall processor im dev ev)))))

(defmethod keyboard-state ((im iron-maiden))
  (--> im 'udev-multi 'keyboard-state))

(defmethod valid-devices ((im iron-maiden))
  (loop for dev in (devices-of im)
        when (valid-device? dev)
        collect dev))

(defmethod reset-keys ((im iron-maiden))
  (loop for key in (im-state:current-key-set (keyboard-state im))
        when (< -1 key)
        do (format t "reset-key: ~A~%" key)))

; }}}

; package iron-maiden-user {{{

(defpackage iron-maiden-user
  (:use :iron-maiden-common :iron-maiden-value :iron-maiden-device :iron-maiden-event :iron-maiden-udev :iron-maiden-finder :iron-maiden-value :iron-maiden
        :common-lisp :sb-gray :alexandria :sb-thread :sb-alien)
  (:nicknames :im-user :imu))
(in-package :iron-maiden-user)


@export
(defun main ()
  (declare (sb-ext:muffle-conditions style-warning))
  (dolist (rc-path (cdr sb-ext:*posix-argv*))
    (if (and rc-path (probe-file rc-path))
      (progn
        (debug-out nil "Loading rc-file: ~A" rc-path)
        (load rc-path))
      (debug-out nil "Not (found|specified) rc-file: ~A~%" rc-path))))


@export
(defmacro next-event () '(throw 'im:next-event "next-event"))
(defmacro next-processor () '(throw 'im:next-processor "next-processor"))

@export
(defmacro event-with (&rest body)
  `(lambda (im dev ev)
     (declare (sb-ext:muffle-conditions style-warning))
     (let ((device-name (im-device:name-of dev))
           (device-class (class-name (class-of dev)))
           (event-type (raw-type-of ev))
           (code (raw-code-of ev))
           (value (raw-value-of ev))
           (msec (destructuring-bind (sec . usec) (raw-time-of ev)
                   (+ (* sec 1000) (round (/ usec 1000))))))
       ,@body)))

@export
(defmacro rhythm-map (key-code rhythm &rest body)
  (with-gensyms (prevs wait-for)
    (eval `(defvar ,prevs nil))
    (eval `(defvar ,wait-for nil))
    `(let* ((rhythm ,rhythm)
            (rhythm-length (length rhythm)))
       (if ,wait-for
         (when-key-up
           (when (= code ,wait-for)
             (send-modified-event im ev)
             (send-syn im)
             ,@body
             (setf ,wait-for nil)
             (next-event)))
         (when-key-down
           (if (= code ,key-code)
             (progn
               (setf ,prevs (cons msec ,prevs))
               (when (< rhythm-length (length ,prevs))
                 (let* ((scale (car rhythm))
                        (margin (/ scale 10))
                        (ds1 (reverse (subseq (zip-with #'- ,prevs (cdr ,prevs)) 0 rhythm-length)))
                        (base (car ds1))
                        (ds2 (zip-with (lambda (x) (* 1.0 (/ x base))) ds1))
                        (ds3 (zip-with (lambda (x) (* scale x)) ds2))
                        (ok? (loop for d in ds3
                                   for r in rhythm
                                   always (destructuring-bind
                                            (bottom . top) (if (consp r) r (cons
                                                                             (- r margin)
                                                                             (+ r margin)))
                                            (<= bottom d top)))))
                   ; (format t "base = ~A, ds1 = ~A, ds2 = ~A, ds3 = ~A, ok=~A ~%" base ds1 ds2 ds3 ok?)
                   (if (and ok?
                            (< base 750))
                     (progn
                       (setf ,prevs nil)
                       (setf ,wait-for ,key-code))
                     (setf ,prevs (subseq ,prevs 0 (1+ rhythm-length)))))))
             (when ,prevs
               (setf ,prevs nil))))))))

@export
(defmacro iron-maiden (&rest body)
  (with-gensyms (devices processors pre-process)
    `(let ((,devices nil)
           (,processors nil)
           (,pre-process nil))
       (labels
         ((define-device (name finder-type key klass delay)
            (pushf ,devices
                   (make-instance klass
                                  :name name
                                  :finder (make-finder finder-type key)
                                  :delay delay)))
          (define-keyboard (name finder-type key &key delay)
            (define-device name finder-type key 'keyboard-device delay))
          (define-mouse (name finder-type key &key delay)
            (define-device name finder-type key 'mouse-device delay))
          (define-joystick (name finder-type key &key delay)
            (define-device name finder-type key 'joystick-device delay)))
         (macrolet
           ((pre-process (args &rest body)
              `(setf
                 ,',pre-process
                 (lambda (,@args)
                   ,@body)))
            (define-processor (&rest body)
              `(pushf ,',processors
                      (event-with
                        ,@body)))
            (pass ()
              `(iron-maiden:send-modified-event im ev))
            ; only in 'event-with
            (when-key-press (&rest body)
              `(when (and (= event-type imv:ev-key)
                          (find value (list imv:value-down imv:value-up imv:value-repeat)))
                 ,@body))
            (when-key-down (&rest body)
              `(when (and (= event-type imv:ev-key)
                          (= value imv:value-down))
                 ,@body))
            (when-key-up (&rest body)
              `(when (and (= event-type imv:ev-key)
                          (= value imv:value-up))
                 ,@body))
            (when-mouse-event (&rest body)
              `(when (= event-type imv:ev-rel)
                 ,@body))
            (change (from to)
              `(when-key-press
                 (when (= ,from code)
                   (im-event:update-event-value ev 'code ,to)
                   (next-processor))))
            (swap (left right)
              `(when-key-press
                 (when (= ,left code)
                   (im:send-modified-event im ev :code ,right)
                   (next-event))
                 (when (= ,right code)
                   (im:send-modified-event im ev :code ,left)
                   (next-event))))
            (for-device (name &rest body)
              (if (listp name)
                `(when (find device-name (list ,@name) :test #'equal) ,@body)
                `(when (equal device-name ,name) ,@body)))
            (for-keyboard (&rest body)
              `(when (typep dev 'im-device:keyboard-device)
                 ,@body))
            (for-window-class (name &rest body)
              (typecase name
                (atom `(when (equal ,name (iron-maiden-x:get-current-window-property :WM_CLASS))
                         ,@body))
                (list `(when (find (iron-maiden-x:get-current-window-property :WM_CLASS)
                                   (list ,@name)
                                   :test #'equal)
                         ,@body)))))
           ; user definition
           ,@body
           ; default pass processor
           (define-processor
             (pass))
           (let ((im (make-instance 'im:iron-maiden
                                    :devices ,devices
                                    :processors ,processors)))
             (when ,pre-process
               (funcall ,pre-process im))
             (im:start im)))))))

; }}}
