
iron-maiden: iron-maiden.cl make.cl iron-maiden.so quicklisp
	sbcl  --no-userinit --no-sysinit --non-interactive \
	      --load ./quicklisp/setup.lisp \
	      --load make.cl

quicklisp: quicklisp.lisp
	(cat quicklisp.lisp && echo '(quicklisp-quickstart:install :path #P"quicklisp/")') | sbcl --no-userinit

quicklisp.lisp:
	wget http://beta.quicklisp.org/quicklisp.lisp

iron-maiden.so: iron-maiden.c
	gcc -shared -o iron-maiden.so -fPIC iron-maiden.c

test:
	sbcl --script iron-maiden.test.cl

clean:
	- rm quicklisp.lisp
	- rm -rf quicklisp
	- rm iron-maiden.so
	- rm iron-maiden

console:
	sbcl  --no-userinit --no-sysinit \
	      --load ./quicklisp/setup.lisp \
	      --eval '(setf ql:*quicklisp-home* (asdf::truenamize #P"quicklisp/"))' \
	      --load iron-maiden.cl
