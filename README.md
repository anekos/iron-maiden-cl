# iron-maiden-clとは
iron-maiden-cl は Linux で動作するキーリマップソフトです。
- SandS のようなトリッキーな設定がxmodmapより容易に実現できる
- X window system のない環境でも動作する
- キーボード毎に設定を使い分けることができる
といった特徴があります。

# Install方法

## Linux 一般

1. sbcl (Steel Bank Common Lisp)
2. gcc
3. wget
4. inotify-tools

## ubuntu 14.04

### 依存するもののinstall (Debian package)

    sudo apt-get install git sbcl make gcc wget inotify-tools

上記は完璧を期したものですが、ubuntu desktopではinstallの引数はsbclだけで多分大丈夫です。

### iron-maidenのinstall

    git clone https://bitbucket.org/anekos/iron-maiden-cl.git
    cd iron-maiden-cl
    make

makeが終わってiron-maidenというbinary fileができていたらbuild成功です。

# 使用と設定の方法

## 使用方法

    sudo ./iron-maiden 設定ファイル

設定ファイルは複数指定でき、始めのものから順にロードされます。

## 設定ファイルの例([rc.clのような]適当な名前で保存してください)

    (in-package :im-user)

    ; SandSのような動作を実現するmacro, 押しっぱなして他keyと組みあわせて使う場合、
    ; origianl-keyはmodifier-keyとして機能し、単独押下ではoriginal-keyは
    ; change-keyとして機能する。 original-keyとchange-keyが同じになるように設定
    ; したい場合はchange-keyをnilとし省略が可能
    (defmacro x-and-y (original-key change-key modifier-key)
      (with-gensyms (pressing any-key-pressed)
        (eval `(defvar ,pressing nil))
        (eval `(defvar ,any-key-pressed nil))
        `(when-key-press
           (if (= code ,original-key)
             (equal-case
               value
               (imv:value-down
                 (setf ,pressing t)
                 (next-event))
               (imv:value-repeat
                 (next-event))
               (imv:value-up
                 (if ,any-key-pressed
                   (progn
                     (send-event im imv:ev-key ,modifier-key imv:value-up)
                     (setf ,any-key-pressed nil))
                   (progn
                     (let ((send-key (or ,change-key ,original-key)))
                       (send-event im imv:ev-key send-key imv:value-down)
                       (send-event im imv:ev-key send-key imv:value-up))))
                 (setf ,pressing nil)
                 (next-event)))
             (when (and (= value imv:value-down) ,pressing (not ,any-key-pressed))
               (send-event im imv:ev-key ,modifier-key imv:value-down)
               (setf ,any-key-pressed t))))))

    (iron-maiden

      ; iron-maidenを適用するkeyboardの定義が必要。
      ; :pathは/dev/input/by-path下にそれらしいものがあるはずです。
      ; 下記はLet's noteのある機種のkeyboardの:pathになります
      (define-keyboard "lets"
                       :path #P"/dev/input/by-path/platform-i8042-serio-0-event-kbd")
      ; thinkpad-usb-keyboard
      (define-keyboard "thinkpad-usb-keyboard"
                       :path #P"/dev/input/by-path/pci-0000:00:1d.2-usb-0:2:1.0-event-kbd")

      (define-processor
        (for-device "lets"
          ; SandSならぬCandSです
          (x-and-y key-space nil key-leftctrl)
          (change key-capslock key-enter)
          (change key-muhenkan key-leftshift)
          ; 変換keyを右windows keyに。
          ; key-rightmetaなどのkey定義はsourceに含まれているconstants.txt中に記載されています
          (change key-henkan key-rightmeta)
          (change key-katakanahiragana key-rightalt))))

## ubuntu desktopでstartup時からiron-maiden-clを有効にする設定

    sudo visudo -f /etc/sudoers.d/username #このusernameと書いた所は何でも構いません

下記を記入、保存(USERNAMEは有効にしたいuser名に置換してください)

    USERNAME ALL=(ALL:ALL) NOPASSWD:iron-maiden-clのfullpath
    
Applicationsから「Startup Applications」を起動

  1. Add button 押下
  2. Name: 「ironなど適当に」、Command: 「sudo iron-maidenのfullpath rc.clのfullpath」、Comment: 「不要です」
  3. Add button 押下
  4. Close button 押下

以上です。
